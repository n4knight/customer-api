﻿namespace CustomerApi.Extensions
{
    public class ErrorModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}