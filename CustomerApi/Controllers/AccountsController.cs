﻿using Contracts;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Controllers
{
    [Route("api/customers/{customerId}/Account")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IRepositoryManager _repoManager;

        public AccountsController(IRepositoryManager _repoManager)
        {
            this._repoManager = _repoManager;
        }


        public IEnumerable<Account> GetAccounts() =>
            _repoManager.Account.GetAccounts();

        [Route("{id}")]
        [HttpGet]
        public Account GetAccount(Guid id) =>
            _repoManager.Account.GetAccountById(id);

        [HttpGet]
        public IActionResult GetCustomerAccount(Guid customerId)
        {
            var customer = _repoManager.Customer.GetCustomerById(customerId);
            if (customer is null) return NotFound();

            var customerAccount = _repoManager.Account.GetAccountById(customer.AccountId);
            if (customerAccount is null) return NotFound();

            return Ok(customerAccount);
        }

        [HttpDelete]
        public IActionResult DeleteAccount(Guid customerId)
        {
            var customer = _repoManager.Customer.GetCustomerById(customerId);
            if (customer is null) return NotFound();

            var customerAccount = _repoManager.Account.GetAccountById(customer.AccountId);
            if (customerAccount is null) return NotFound();

            _repoManager.Account.DeleteAccount(customerAccount);
            _repoManager.Save();

            return NoContent();
        }
    }
}
