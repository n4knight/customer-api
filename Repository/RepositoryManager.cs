﻿using Contracts;
using Entities;
using System;


namespace Repository
{
    public class RepositoryManager: IRepositoryManager
    {
        private readonly RepositoryContext _context;
        private  ICustomerRepository _customerRepo;
        private  IAccountRepository _accountRepo;

        public RepositoryManager(RepositoryContext _context)
        {
            this._context = _context;
        }

        public ICustomerRepository Customer
        {
            get
            {
                if (_customerRepo is null)
                    _customerRepo = new CustomerRepository(_context);
                return _customerRepo;
            }
        }

        public IAccountRepository Account
        {
            get
            {
                if (_accountRepo is null)
                    _accountRepo = new AccountRepository(_context);
                return _accountRepo;
            }
        }

        public void Save() => _context.SaveChanges();
    }
}
