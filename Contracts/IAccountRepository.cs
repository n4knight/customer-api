﻿using Entities;
using System.Collections.Generic;
using System;

namespace Contracts
{
    public interface IAccountRepository
    {
        IEnumerable<Account> GetAccounts();

        Account GetAccountById(Guid id);

        void DeleteAccount(Account account2Remove);

    }
}