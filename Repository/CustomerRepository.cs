﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CustomerRepository: RepositoryBase<Customer>, ICustomerRepository
    {
        private readonly RepositoryContext _context;

        public CustomerRepository(RepositoryContext _context):base(_context)
        {
            this._context = _context;
        }

        public void DeleteCustomer(Customer customer2Remove) =>
            Delete(customer2Remove);

        public Customer GetCustomerById(Guid id) =>
            FindByCondition(c => c.Id == id).FirstOrDefault();

        public IEnumerable<Customer> GetCustomers() =>
            FindAll()
            .OrderBy(c => c.Id)
            .ToList();

        
    }
}
