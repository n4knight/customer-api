﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class AccountRepository: RepositoryBase<Account>, IAccountRepository
    {
        private readonly RepositoryContext _context;

        public AccountRepository(RepositoryContext _context):base(_context)
        {
            this._context = _context;
        }

        public void DeleteAccount(Account account2Remove) =>
            Delete(account2Remove);

        public Account GetAccountById(Guid id) =>
            FindByCondition(a => a.Id == id).FirstOrDefault();

        public IEnumerable<Account> GetAccounts() =>
            FindAll()
            .OrderBy(a => a.Balance)
            .ToList();

    }
}
