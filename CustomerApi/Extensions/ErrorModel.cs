﻿namespace CustomerApi.Extensions
{
    internal class ErrorModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}