﻿using Contracts;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IRepositoryManager _repoManager;

        public CustomersController(IRepositoryManager _repoManager)
        {
            this._repoManager = _repoManager;
        }


        public IEnumerable<Customer> GetCustomers() =>
            _repoManager.Customer.GetCustomers();


        [Route("{id}")]
        [HttpGet]
        public IActionResult GetCustomer(Guid id)
        {
            var customer = _repoManager.Customer.GetCustomerById(id);
            if (customer is null) return NotFound();

            var customerAccount = _repoManager.Account.GetAccountById(customer.AccountId);
            if (customerAccount is null) return NotFound();

            customer.Account = new Account
            {
                Id = customerAccount.Id,
                AccountNumber = customerAccount.AccountNumber,
                Balance = customerAccount.Balance
            };

            return Ok(customer);
        }

        [HttpDelete("id")]
        public IActionResult DeleteCustomer(Guid id)
        {
            var customer = _repoManager.Customer.GetCustomerById(id);
            if (customer is null) return NotFound();

            _repoManager.Customer.DeleteCustomer(customer);
            _repoManager.Save();

            return NoContent();


        }



    }
}
